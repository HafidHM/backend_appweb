package com.sid.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * BackendAppwebApplicationTests is the class main for the tests in the project
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@SpringBootTest
class BackendAppwebApplicationTests {

	@Test
	void contextLoads() {
	}

}
