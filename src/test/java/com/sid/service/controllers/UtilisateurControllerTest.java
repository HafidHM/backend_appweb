package com.sid.service.controllers;

import com.sid.service.dao.RoleRepository;
import com.sid.service.dao.SeanceRepository;
import com.sid.service.dao.UtilisateurRepository;
import com.sid.service.entities.*;
import static org.assertj.core.api.Assertions.assertThat;
import com.sid.service.services.UtilisateurService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import java.sql.Date;
import java.util.List;


/**
 * Class for testing the methodes in the Controller Class
 * You can get, update, delete and create an object ( Fichier, Matiere, Place, Role, Seance, Utilisateur)
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(value = "test")
@AutoConfigureMockMvc(addFilters = false)
class UtilisateurControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UtilisateurService utilisateurService;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private SeanceRepository seanceRepository;

    @Autowired
    private RoleRepository roleRepository;



    Utilisateur uts = new Utilisateur("test1",
            "test1", "0677334455","test@gmail.com",
            "test",
            "test",true);


    /**
     * Testing: create, get by id and delete user object
     */
    @Test
    void test_findByUsernameContains(){
        //Create
        utilisateurService.createUtilisateur(uts);
        //Get
        List <Utilisateur> u =  utilisateurRepository.findByUsernameContains("test");
        assertThat(u.size()).isGreaterThan(0);
        //Delete
        String message = utilisateurService.deleteUserById(u.get(0).getIdU());
        assertThat(message).isEqualTo("User successfully deleted");
    }


    /**
     * Testing: create, get and delete user object
     */
    @Test
    void test_get_delete_create_user(){
        //Create
        Role role = new Role("TEST1");
        utilisateurService.createRole("TEST1");
        //Get
        List <Role>r = utilisateurService.findAllRoles();
        int size = r.size();
        assertThat(r.get(size-1).getRole()).isEqualTo("TEST1");
        //Delete
        int m = roleRepository.deleteByRole("TEST1");
        assertThat(m).isEqualTo(1);
    }


    /**
     * Testing: create, get, delete and update user object
     */
    @Test
    void test_create_find_update_user() {
        //Create
        utilisateurService.createUtilisateur(uts);
        //Get
        List<Utilisateur> users = utilisateurRepository.findByUsernameContains("test");
        assertThat(users).isNotNull();

        List <Utilisateur> u =  utilisateurRepository.findByUsernameContains("test");
        //Update
        u.get(0).setMailU("test1@gmail.com");
        utilisateurService.UpdateUser(u.get(0));
        u =  utilisateurRepository.findByUsernameContains("test");
        assertThat(u.get(0).getMailU()).isEqualTo("test1@gmail.com");
        //Delete
        String message = utilisateurService.deleteUserById(u.get(0).getIdU());
        assertThat(message).isEqualTo("User successfully deleted");
    }

    /**
     * Testing: create, get, delete and update seance object
     */
    @Test
    void test_create_get_update_delete_seance(){
        Seance seance = new Seance();
        seance.setIdP((long) 2);
        seance.setIdM((long) 2);
        seance.setNbH((long) 45);
        seance.setDescriptionS("test");
        seance.setEtat((long) 1);
        seance.setNbp((long) 22);
        seance.setLien("test");
        seance.setDateS(Date.valueOf("2021-09-21"));
        //create
        utilisateurService.createSeance(seance);
        List <Seance>ss = utilisateurService.findAllSeance();
        assertThat(ss.contains(seance)).isEqualTo(true);

        //Update
        seance.setEtat((long) 0);
        utilisateurService.UpdateSeance(seance);
        ss = utilisateurService.findAllSeance();
        assertThat(ss.contains(seance)).isEqualTo(true);

        int size = ss.size()-1;
        //Delete
        List <Seance>sw = utilisateurService.findAllSeance();
        String message = utilisateurService.deleteSeanceById(sw.get(size).getIdS());
        assertThat(message).isEqualTo("Seance successfully deleted");
    }

    /**
     * Testing: create, get, delete and update place object
     */
    @Test
    void test_create_get_update_delete_place() {
        Place place = new Place();
        place.setIdP((long) 2);
        place.setIdS((long) 11);
        place.setIdA((long) 4);
        place.setDateR(Date.valueOf("2021-11-21"));
        //Create
        utilisateurService.createPlace(place);
        //Get
        List <Place> ps = utilisateurService.findAllPlace();
        assertThat(ps.contains(place)).isEqualTo(true);

        //Update
        place.setDateR(Date.valueOf("2000-11-21"));
        utilisateurService.UpdatePlace(place);
        ps = utilisateurService.findAllPlace();
        assertThat(ps.contains(place)).isEqualTo(true);


        int size = ps.size()-1;
        //Delete
        List <Place>sw = utilisateurService.findAllPlace();
        String message = utilisateurService.deletePlaceById(ps.get(size).getId());
        assertThat(message).isEqualTo("Place successfully deleted");
    }

    /**
     * Testing: create, get, delete and update matiere object
     */
    @Test
    void test_create_get_update_delete_matiere() {
        //Create
        utilisateurService.createMatiere("test","test description");
        //Get
        List<Matiere> m = utilisateurService.findAllMatiers();
        assertThat(m.size()).isGreaterThan(0);
        int size = m.size();
        //Update
          m.get(size-1).setDescriptionM("test 2");
          utilisateurService.UpdateMatiere(m.get(size-1));
          m = utilisateurService.findAllMatiers();
          assertThat(m.get(size-1).getDescriptionM()).isEqualTo("test 2");
        //Delete
        String message = utilisateurService.deleteMatiereById(m.get(size-1).getIdM());
        assertThat(message).isEqualTo("Matiere successfully deleted");
    }



    /**
     * Testing: create, get and delete file object
     */
    @Test
    void test_create_get_delete_files() {
        Fichier f = new Fichier();
        f.setDescriptionF("test");
        f.setNomF("test");
        f.setIdM((long) 2);
        f.setIdP((long) 2);
        utilisateurService.createFile(f);
        List<Fichier> files = utilisateurService.findAllFiles();
        int size = files.size();
        assertThat(files.size()).isGreaterThan(0);
        //Delete
        String message = utilisateurService.deleteFileById(files.get(size-1).getIdF());
        assertThat(message).isEqualTo("File successfully deleted");

    }


//    @Test
//    void test_addRoleToUser() {
//        //utilisateurService.createUtilisateur(uts);
//        utilisateurService.addRoleToUser("test","APPRENANT");
////        List<Utilisateur> u =  utilisateurRepository.findByUsernameContains("test");
////        assertThat(u.get(0).getRoles().contains("APPRENANT")).isEqualTo(true);
////        utilisateurService.deleteUserById(u.get(0).getIdU());
//    }


}