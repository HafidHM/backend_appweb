package com.sid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import javax.sql.DataSource;


/**
 *  Class for security, configuration and authentification with the mysql database
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Profile(value = {"!test"})
@CrossOrigin(origins = {"http://localhost:8090/","http://localhost:3306/"} )
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/css/**","/js/**","/images/**","/test/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/index.html")
                .and()
                .logout()
                    .permitAll()
                    .logoutUrl("/logout")
                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/403");


    }

    @Autowired
    public void globalConfig(AuthenticationManagerBuilder auth, DataSource dataSource) throws Exception {
          auth.jdbcAuthentication()
                  .passwordEncoder(new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 10)).dataSource(dataSource)
          .usersByUsernameQuery("select username as principal, password as credentials, actived is true from utilisateur where username = ?")
                  .authoritiesByUsernameQuery("select B.username as principal, A.roles_role as role from users_roles as A join utilisateur B on (A.utilisateur_idu = B.idU)  where B.username = ?")
                  .rolePrefix("ROLE_");
    }

}
