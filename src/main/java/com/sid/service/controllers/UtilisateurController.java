package com.sid.service.controllers;
import com.sid.service.entities.*;
import com.sid.service.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Class Control the communication between the client and server
 * You can get, update, delete and create an object ( Fichier, Matiere, Place, Role, Seance, Utilisateur)
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@CrossOrigin(origins = {"http://localhost:8090/","http://localhost:3306/"} )
@RestController
@Validated
public class UtilisateurController {

    @Autowired
    private UtilisateurService utilisateurService;

    /**
     * Methode to create a User object
     * @param u The user created
     * @return Return the new User created
     */
    @PostMapping("user/create")
    public Utilisateur create( @RequestBody Utilisateur u){
        return utilisateurService.createUtilisateur(u);
    }

    /**
     * Methode to delete a User object by id
     * @param idU The id user to delete
     * @return Return a message
     */
    @DeleteMapping("user/delete/{idU}")
    public String deleteUser(@PathVariable Long idU) {
        return utilisateurService.deleteUserById(idU);
    }

    /**
     * Methode to update a User object
     * @param newU The new user to update
     * @return Return the new user
     */
    @PutMapping("user/update")
    public Utilisateur updateUser(@RequestBody Utilisateur newU) {
        return utilisateurService.UpdateUser(newU);
    }

    /**
     * Methode to search a User by id
     * @param id The id user to search
     * @return Return list of users
     */
    @GetMapping("/user/{id}")
    public Optional<Utilisateur> findUserById(@PathVariable Long id) {
        return utilisateurService.findUserById(id);
    }


    /**
     * Methode to get list of User object
     * @return Return list of users
     */
    @GetMapping("/users")
    public List<Utilisateur> getUsers(){
        List<Utilisateur> u= utilisateurService.findAllUsers();
        return u;
    }

    /**
     * Methode to get current session information
     * @param  httpServletRequest HttpServletRequest object
     * @return Return session information
     */
    @GetMapping("user/getLogedUser")
    public Map<String,Object> getLogedUser(HttpServletRequest httpServletRequest){
        return utilisateurService.getLogedUser(httpServletRequest);
    }

    /**
     * Methode to get a list of role object
     * @return Return list role object
     */
    @GetMapping("/roles")
    public List<Role> getRoles(){
        return utilisateurService.findAllRoles();
    }

    /**
     * Methode to create a role object
     * @param r The name of role to create
     * @return Return the new role created
     */
    @GetMapping("role/create")
    @ResponseBody
    public Role createRole(@RequestParam String  r){
         return utilisateurService.createRole(r);
    }



//    @GetMapping("user/addRoleToUser")
//    public String addRoleToUser(String username, String role ){
//        return utilisateurService.addRoleToUser(username, role);
//    }

    /**
     * Methode to create a matiere object
     * @param nomM The name of matiere object to create
     * @param descriptionM The description of matiere object to create
     * @return Return the new matiere object created
     */
    @GetMapping("matiere/create")
    @ResponseBody
    public Matiere create(@RequestParam String nomM, @RequestParam String descriptionM){
        return utilisateurService.createMatiere(nomM,descriptionM);
    }

    /**
     * Methode to delete a matiere object by id
     * @param idM The id of matiere object to delete
     * @return Return message
     */
    @DeleteMapping("matiere/delete/{idM}")
    public String deleteMatiere(@PathVariable Long idM) {
        return utilisateurService.deleteMatiereById(idM);
    }

    /**
     * Methode to get a list of matiere object
     * @return Return list of matiere object
     */
    @GetMapping("/matiers")
    public List<Matiere> getMatieres(){
        List<Matiere> m= utilisateurService.findAllMatiers();
        return m;
    }

    /**
     * Methode to update a matiere object
     * @param newM The new matiere object to update
     * @return Return a new matiere object
     */
    @PutMapping("matiere/update")
    public Matiere updateMatiere(@RequestBody Matiere newM) {
        return utilisateurService.UpdateMatiere(newM);
    }

    /**
     * Methode to get a list of file object
     * @return Return list of file object
     */
    @GetMapping("/files")
    public List<Fichier> getFiles(){
        return utilisateurService.findAllFiles();
    }


    /**
     * Methode to create a file object
     * @param f The file object to create
     * @return Return the new file object created
     */
    @PostMapping("file/create")
    public Fichier createFile( @RequestBody Fichier f){
        return utilisateurService.createFile(f);
    }

    /**
     * Methode to delete a file object by id
     * @param idF The id file object to delete
     * @return Return message
     */
    @DeleteMapping("file/delete/{idF}")
    public String deleteFile(@PathVariable Long idF) {
        return utilisateurService.deleteFileById(idF);
    }

    /**
     * Methode to get a list of seance object
     * @return Return list of seance object
     */
    @GetMapping("/seances")
    public List<Seance> getSeance(){
        return utilisateurService.findAllSeance();
    }

    /**
     * Methode to create a seance object
     * @param s The seance object to create
     * @return Return the new seance object created
     */
    @PostMapping("seance/create")
    public Seance createSeance(@RequestBody Seance s){
        return utilisateurService.createSeance(s);
    }

    /**
     * Methode to delete a seance object by id
     * @param idS The id of seance object to delete
     * @return Return message
     */
    @DeleteMapping("seance/delete/{idS}")
    public String deleteSeance(@PathVariable Long idS ) {
        return utilisateurService.deleteSeanceById(idS);
    }

    /**
     * Methode to update a seance object
     * @param newS The new seance object to update
     * @return Return a new seance object
     */
    @PutMapping("seance/update")
    public Seance updateSeance(@RequestBody Seance newS) {
        return utilisateurService.UpdateSeance(newS);
    }

    /**
     * Methode to get a list of place object
     * @return Return list of place object
     */
    @GetMapping("/places")
    public List<Place> getPlace(){
        return utilisateurService.findAllPlace();
    }

    /**
     * Methode to create a place object
     * @param p The place object to create
     * @return Return the new place object created
     */
    @PostMapping("place/create")
    public Place createPlace(@RequestBody Place p){
        return utilisateurService.createPlace(p);
    }

    /**
     * Methode to delete a place object by id
     * @param id The id place object to delete
     * @return Return message
     */
    @DeleteMapping("place/delete/{id}")
    public String deletePlace(@PathVariable Long id) {
        return utilisateurService.deletePlaceById(id);
    }

    /**
     * Methode to update a place object
     * @param newP The new place object to update
     * @return Return a new place object
     */
    @PutMapping("place/update")
    public Place updatePlace(@RequestBody Place newP) {
        return utilisateurService.UpdatePlace(newP);
    }

}
