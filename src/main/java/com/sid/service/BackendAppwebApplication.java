package com.sid.service;

import com.sid.service.dao.*;
import com.sid.service.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
/**
 * BackendAppwebApplication is the class main for the project
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */

@SpringBootApplication
public class BackendAppwebApplication implements CommandLineRunner {
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	private FichierRepository fichierRepository;
	private MatiereRepository matiereRepository;
	private PlaceRepository placeRepository;
	private SeanceRepository seanceRepository;

	@Autowired
	private RepositoryRestConfiguration restConfiguration;

	public static void main(String[] args)  {
		ApplicationContext ctx = SpringApplication.run(BackendAppwebApplication.class, args);
		UtilisateurRepository utilisateurRepository = ctx.getBean(UtilisateurRepository.class);
	}


	@Override
	public void run(String... args) throws Exception {
		restConfiguration.exposeIdsFor(Utilisateur.class);
		restConfiguration.exposeIdsFor(Fichier.class);
		restConfiguration.exposeIdsFor(Matiere.class);
		restConfiguration.exposeIdsFor(Place.class);
		restConfiguration.exposeIdsFor(Seance.class);
		restConfiguration.exposeIdsFor(Role.class);
	}
}

