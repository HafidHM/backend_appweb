package com.sid.service.dao;
import com.sid.service.entities.Seance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Interface use JpaRepository options
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@RepositoryRestResource
public interface SeanceRepository  extends JpaRepository<Seance,Long> {

    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    @RestResource(path = "/byIdP")
    public List<Seance> findByIdPEquals(@Param("idP") Long idP);

    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    @RestResource(path = "/byIdM")
    public List<Seance> findByIdMEquals(@Param("idM") Long idM);

}
