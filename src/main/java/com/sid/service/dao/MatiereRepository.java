package com.sid.service.dao;
import com.sid.service.entities.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Interface use JpaRepository options
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@RepositoryRestResource
public interface MatiereRepository  extends JpaRepository<Matiere,Long> {

    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    @RestResource(path = "/byNomM")
    public List<Matiere> findByNomMContains(@Param("NomM") String NomM);
}
