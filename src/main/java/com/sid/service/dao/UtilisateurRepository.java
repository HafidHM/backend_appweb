package com.sid.service.dao;
import com.sid.service.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.List;
@CrossOrigin(origins = {"http://localhost:8090/","http://localhost:3306/"} )
/**
 * Interface use JpaRepository options
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@RepositoryRestResource
public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long> {

    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    @RestResource(path = "/byUsername")
    public List<Utilisateur> findByUsernameContains(@Param("username") String username);

}
