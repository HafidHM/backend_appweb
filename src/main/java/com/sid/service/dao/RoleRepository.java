package com.sid.service.dao;
import com.sid.service.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * Interface use JpaRepository options
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@RepositoryRestResource
public interface RoleRepository  extends JpaRepository<Role,Long> {

    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    @RestResource(path = "/role")
    public List<Role> findByRoleContains(@Param("r") String role);

    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    @Transactional
    @Modifying
    @Query("delete from Role a where a.role = :r")
    Integer deleteByRole(String r);

}
