package com.sid.service.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


/**
 * Class to create and manage Utilisateur object
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@Entity
@Data
@NoArgsConstructor
@ToString
@Table(name="utilisateur")
public class Utilisateur implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idU;
    private String nomU;
    private String prenomU;
    @ManyToMany
    @JoinTable (name = "USERS_ROLES")
    private Collection<Role> roles;
    private String telU;
    private String mailU;
    private String username;
    private String password;
    private boolean actived;


    public Utilisateur(Long idU, String nomU, String prenomU, Collection<Role> roles, String telU, String mailU, String username, String password, boolean actived) {
        this.idU = idU;
        this.nomU = nomU;
        this.prenomU = prenomU;
        this.roles = roles;
        this.telU = telU;
        this.mailU = mailU;
        this.username = username;
        this.password = password;
        this.actived = actived;
    }
    public Utilisateur(String nomU, String prenomU,  String telU, String mailU, String username, String password, boolean actived) {
        this.nomU = nomU;
        this.prenomU = prenomU;
        this.telU = telU;
        this.mailU = mailU;
        this.username = username;
        this.password = password;
        this.actived = actived;
    }



    public boolean isActived() {
        return actived;
    }

    public void setActived(boolean actived) {
        this.actived = actived;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Long getIdU() {
        return idU;
    }

    public void setIdU(Long idU) {
        this.idU = idU;
    }

    public String getNomU() {
        return nomU;
    }

    public void setNomU(String nomU) {
        this.nomU = nomU;
    }

    public String getPrenomU() {
        return prenomU;
    }

    public void setPrenomU(String prenomU) {
        this.prenomU = prenomU;
    }

    public String getTelU() {
        return telU;
    }

    public void setTelU(String telU) {
        this.telU = telU;
    }

    public String getMailU() {
        return mailU;
    }

    public void setMailU(String mailU) {
        this.mailU = mailU;
    }













}
