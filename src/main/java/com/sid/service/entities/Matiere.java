package com.sid.service.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class to create and manage Matiere object
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Matiere implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idM;
    private String nomM;
    private String descriptionM;

    public Long getIdM() {
        return idM;
    }

    public Matiere(String nomM, String descriptionM) {
        this.nomM = nomM;
        this.descriptionM = descriptionM;
    }

    public void setIdM(Long idM) {
        this.idM = idM;
    }

    public String getNomM() {
        return nomM;
    }

    public void setNomM(String nomM) {
        this.nomM = nomM;
    }

    public String getDescriptionM() {
        return descriptionM;
    }

    public void setDescriptionM(String descriptionM) {
        this.descriptionM = descriptionM;
    }



}

