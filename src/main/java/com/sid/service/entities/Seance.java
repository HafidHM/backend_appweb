package com.sid.service.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.type.DateType;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;


/**
 * Class to create and manage Seance object
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Seance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idS;
    private Long idP;
    private Long idM;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date dateS = new Date();
    private Long nbH;
    private Long nbp;
    private String descriptionS;
    private Long etat ;
    private String lien;

    public Seance(Long idP, Long idM, Date dateS, Long nbH, Long nbp, String descriptionS, Long etat, String lien) {
        this.idP = idP;
        this.idM = idM;
        this.dateS = dateS;
        this.nbH = nbH;
        this.nbp = nbp;
        this.descriptionS = descriptionS;
        this.etat = etat;
        this.lien = lien;
    }


    public Long getIdS() {
        return idS;
    }

    public void setIdS(Long idS) {
        this.idS = idS;
    }

    public Long getIdP() {
        return idP;
    }

    public void setIdP(Long idP) {
        this.idP = idP;
    }

    public Long getIdM() {
        return idM;
    }

    public void setIdM(Long idM) {
        this.idM = idM;
    }

    public Date getDateS() {
        return dateS;
    }

    public void setDateS(Date dateS) {
        this.dateS = dateS;
    }

    public Long getNbH() {
        return nbH;
    }

    public void setNbH(Long nbH) {
        this.nbH = nbH;
    }

    public Long getNbp() {
        return nbp;
    }

    public void setNbp(Long nbp) {
        this.nbp = nbp;
    }

    public String getDescriptionS() {
        return descriptionS;
    }

    public void setDescriptionS(String descriptionS) {
        this.descriptionS = descriptionS;
    }

    public Long getEtat() {
        return etat;
    }

    public void setEtat(Long etat) {
        this.etat = etat;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }



}

