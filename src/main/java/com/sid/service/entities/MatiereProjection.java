package com.sid.service.entities;

import org.springframework.data.rest.core.config.Projection;

/**
 * Interface to create a projection on the Matiere object list
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */
@Projection(name = "noms", types = Matiere.class)
public interface MatiereProjection {
    public  String getNomM();
}
