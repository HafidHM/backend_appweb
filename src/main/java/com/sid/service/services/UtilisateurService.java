package com.sid.service.services;

import com.sid.service.dao.*;
import com.sid.service.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Class Service hase  all methodes to communicate with the database
 * You can get, update, delete and create an object ( Fichier, Matiere, Place, Role, Seance, Utilisateur)
 *
 * @author      HAFID Mohamed
 * @version     %I%, %G%
 * @since       1.0
 */

@CrossOrigin(origins = {"http://localhost:8090/","http://localhost:3306/"} )
@Service
public class UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MatiereRepository matiereRepository;
    @Autowired
    private FichierRepository fichierRepository;
    @Autowired
    private SeanceRepository seanceRepository;
    @Autowired
    private PlaceRepository placeRepository;

    /**
     * Methode to create a User object
     * @param u The user created
     * @return Return the new User created
     */
    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    public Utilisateur createUtilisateur(Utilisateur u){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 10);
        u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));
        return utilisateurRepository.save(u);
    }


    /**
     * Methode to search a User by id
     * @param id The id user to search
     * @return Return list of users
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public Optional<Utilisateur> findUserById(Long id) {
        return utilisateurRepository.findById(id);
    }


    /**
     * Methode to get list of User object
     * @return Return list of users
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public List<Utilisateur> findAllUsers(){
        List<Utilisateur> u= utilisateurRepository.findAll();
        return u;
    }

    /**
     * Methode to get a list of matiere object
     * @return Return list of matiere object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public List<Matiere> findAllMatiers(){
        List<Matiere> m= matiereRepository.findAll();
        return m;
    }


    /**
     * Methode to delete a User object by id
     * @param id The id user to delete
     * @return Return a message
     */
    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    public String deleteUserById(@PathVariable("id") Long id) {
         utilisateurRepository.deleteById(id);
         return "User successfully deleted";
    }



    /**
     * Methode to update a User object
     * @param newU The new user to update
     * @return Return a new user
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public Utilisateur UpdateUser(Utilisateur newU) {
        Utilisateur u = utilisateurRepository.findById(newU.getIdU()).orElse(null);
        u.setNomU(newU.getNomU());
        u.setPrenomU(newU.getPrenomU());
        u.setMailU(newU.getMailU());
        u.setTelU(newU.getTelU());
        u.setPassword(newU.getPassword());
        u.setActived(newU.isActived());
        return utilisateurRepository.save(u);
    }

    /**
     * Methode to get current session information
     * @param  httpServletRequest HttpServletRequest object
     * @return Return session information
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public Map<String,Object> getLogedUser(HttpServletRequest httpServletRequest){
        HttpSession httpSession = httpServletRequest.getSession();
        SecurityContext securityContext = (SecurityContext) httpSession.getAttribute("SPRING_SECURITY_CONTEXT");
        String username = securityContext.getAuthentication().getName();
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority ga: securityContext.getAuthentication().getAuthorities()){roles.add(ga.getAuthority()); }
        Map<String,Object > params = new HashMap<>();
        params.put("roles",roles);
        params.put("username",username);
        return params;
    }

    /**
     * Methode to get a list of role object
     * @return Return list role object
     */
    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    public List<Role> findAllRoles(){
        return roleRepository.findAll();
    }


    /**
     * Methode to create a role object
     * @param t The name of role to create
     * @return Return the new role created
     */
    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    public Role createRole(String t){
        return roleRepository.save(new Role(t));
      }


//    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
//    public String addRoleToUser(String username, String role ){
//        List<Utilisateur> utilisateurs= utilisateurRepository.findByUsernameContains(username);
//        List<Role> roles = roleRepository.findByRoleContains(role);
//        for (Utilisateur u: utilisateurs) {
//            for (Role r: roles) {
//                u.getRoles().add(r);
//                UpdateUser(u);
//            }
//        }
//        return "Role added successfully";
//    }


    /**
     * Methode to create a matiere object
     * @param nomM The name of matiere object to create
     * @param descriptionM The description of matiere object to create
     * @return Return the new matiere object created
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public  Matiere createMatiere(String nomM, String descriptionM){
        return matiereRepository.save(new Matiere(nomM,descriptionM));
    }

    /**
     * Methode to delete a matiere object by id
     * @param id The id of matiere object to delete
     * @return Return message
     */
    @Secured(value={"ROLE_ADMIN"/*,"ROLE_PROFESSEUR","ROLE_APPRENANT"*/})
    public String deleteMatiereById(@PathVariable("id") Long id) {
        matiereRepository.deleteById(id);
        return "Matiere successfully deleted";
    }

    /**
     * Methode to get a list of file object
     * @return Return list of file object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public List<Fichier> findAllFiles(){
        return fichierRepository.findAll();
    }

    /**
     * Methode to create a file object
     * @param f The file object to create
     * @return Return the new file object created
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public Fichier createFile( Fichier f){
        return fichierRepository.save(f);
    }

    /**
     * Methode to delete a file object by id
     * @param idF The id file object to delete
     * @return Return message
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public String deleteFileById(@PathVariable Long idF) {
         fichierRepository.deleteById(idF);
        return "File successfully deleted";
    }

    /**
     * Methode to get a list of seance object
     * @return Return list of seance object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public List<Seance> findAllSeance(){
        return seanceRepository.findAll();
    }

    /**
     * Methode to create a seance object
     * @param s The seance object to create
     * @return Return the new seance object created
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public Seance createSeance(Seance s){
        return seanceRepository.save(s);
    }

    /**
     * Methode to delete a seance object by id
     * @param idS The id of seance object to delete
     * @return Return message
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public String deleteSeanceById(@PathVariable("id") Long idS) {
        seanceRepository.deleteById(idS);
        return "Seance successfully deleted";
    }

    /**
     * Methode to update a seance object
     * @param newS The new seance object to update
     * @return Return a new seance object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public Seance UpdateSeance(Seance newS) {
        Seance s = seanceRepository.findById(newS.getIdS()).orElse(null);
        s.setIdP(newS.getIdP());
        s.setIdS(newS.getIdS());
        s.setIdM(newS.getIdM());
        s.setEtat(newS.getEtat());
        s.setDescriptionS(newS.getDescriptionS());
        s.setNbH(newS.getNbH());
        s.setDateS(newS.getDateS());
        s.setNbp(newS.getNbp());
        s.setLien(newS.getLien());
        return seanceRepository.save(s);
    }


    /**
     * Methode to update a matiere object
     * @param newM The new matiere object to update
     * @return Return a new matiere object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR"/*,"ROLE_APPRENANT"*/})
    public Matiere UpdateMatiere(Matiere newM) {
        Matiere m = matiereRepository.findById(newM.getIdM()).orElse(null);
        m.setNomM(newM.getNomM());
        m.setDescriptionM(newM.getDescriptionM());
        return matiereRepository.save(m);
    }

    /**
     * Methode to get a list of place object
     * @return Return list of place object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public List<Place> findAllPlace(){
        return placeRepository.findAll();
    }

    /**
     * Methode to create a place object
     * @param p The place object to create
     * @return Return the new place object created
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public Place createPlace(Place p){
        return placeRepository.save(p);
    }

    /**
     * Methode to delete a place object by id
     * @param id The id place object to delete
     * @return Return message
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public String deletePlaceById(@PathVariable("id") Long id) {
        placeRepository.deleteById(id);
        return "Place successfully deleted";
    }

    /**
     * Methode to update a place object
     * @param newP The new place object to update
     * @return Return a new place object
     */
    @Secured(value={"ROLE_ADMIN","ROLE_PROFESSEUR","ROLE_APPRENANT"})
    public Place UpdatePlace(Place newP) {
        Place p = placeRepository.findById(newP.getId()).orElse(null);
        p.setIdP(newP.getIdP());
        p.setIdS(newP.getIdS());
        p.setIdA(newP.getIdA());
        p.setDateR(newP.getDateR());
        return placeRepository.save(p);
    }
}
